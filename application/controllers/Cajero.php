<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cajero extends CI_Controller {
	public function __construct(){
		parent:: __construct();
		$this->load->model('cajero_model');
		$this->load->helper('url');
	}

	public function Index(){
		if ($this->session->userdata('rol') == 'Administrador' || $this->session->userdata('rol') == 'Gerente') {
			$data=array(
				'page_title'=>'Cajeros',
				'view'=>'Cajero',
				'data_view'=>array());
			$Cajero = $this->cajero_model->cajero();
			$data['Cajero']= $Cajero;
			$Estado=$this->cajero_model->Estado();
			$data['Estado']=$Estado;
			$this->load->view('template/main', $data);
		}elseif($this->session->userdata('rol') == 'Recepcionista') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'ClienteCtrl';</script>";
		}elseif($this->session->userdata('rol') == 'Gerente') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Usuario/CargarUsuario';</script>";
		}elseif($this->session->userdata('rol') == 'Cajero') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Cajero/cliente';</script>";
		}else{
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Login_Controller/Logear';</script>";
		}
	}

	public function cajero(){
		if ($this->session->userdata('rol')== 'Administrador' || $this->session->userdata('rol')=='Gerente' || $this->session->userdata('rol') == 'Gerente') {
			$data=array(
				'page_title'=>'Agregar_Cajero',
				'view'=>'Agregar_Cajero',
				'data_view'=>array());
			$this->load->view('template/main',$data);
		}elseif($this->session->userdata('rol') == 'Recepcionista') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'ClienteCtrl';</script>";
		}else{
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Login_Controller/Logear';</script>";
		}
		
	}

	public function Agregar_Cajero(){
		if($this->input->is_ajax_request()){
			$data = array(
				'idestado' =>$this->input->post('idestado'),
				'N_Caja'=>$this->input->post('N_Caja')
			);

			if($this->cajero_model->Agregar_Cajero($data))
			{
				echo json_encode(array('success' => 1));
			}
			else
			{
				echo json_encode(array('success' => 0));
			}
		}
		else
		{
			echo "No se puede acceder";
		}

	}
	public function Update_Cajero($valor){
		if ($this->session->userdata('rol')== 'Administrador' || $this->session->userdata('rol')=='Gerente') {

			$data=array(
				'page_title'=>'',
				'view'=>'Actualizar_Cajero',
				'data_view'=>array());
			$Cajero = $this->cajero_model->Obtener_Cajero($valor);
			$data['C']= $Cajero;
			$Estado=$this->cajero_model->Estado();
			$data['Estado']=$Estado;
			$this->load->view('template/main',$data);
		}elseif($this->session->userdata('rol') == 'Recepcionista') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'ClienteCtrl';</script>";
		}elseif($this->session->userdata('rol') == 'Cajero') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Cajero/cliente';</script>";
		}else{
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Login_Controller/Logear';</script>";
		}

	}
	public function Actualizar_Cajero(){
		if($this->input->is_ajax_request()){
			$data=array(
				'Id_Cajero'=>$this->input->post('Id_Cajero'),
				'idestado' =>$this->input->post('idestado'),
				'N_Caja'=>$this->input->post('N_Caja')

			);

			if($this->cajero_model->Actualizar_Cajero($data)){
				echo json_encode(array('success' =>1));
			}
			else
			{
				echo json_encode(array('success' => 0));
			}
		}
		else
		{
			echo "No se pudo acceder";
		}
	}
	/*public function Eliminar($Id_Cajero){
		$this->cajero_model->eliminar($Id_Cajero);
		$this->index();
	}*/

	public function Eliminar(){
		$this->cajero_model->Eliminar();
		echo json_encode(array("status" => TRUE));
	}
////////
	public function cliente(){
		if ($this->session->userdata('rol') == 'Cajero' || $this->session->userdata('rol') == 'Administrador' ||$this->session->userdata('rol') == 'Gerente') {
			$data=array(
				'page_title'=>'cliente','view'=>'cliente','data_view'=>array());

			$abono=$this->cajero_model->llamados('AB');
			$data['abono']=$abono->idcliente;

			$prestamo=$this->cajero_model->llamados('PR');
			$data['prestamo']=$prestamo->idcliente;

			$retiro=$this->cajero_model->llamados('RT');
			$data['retiro']=$retiro->idcliente;

			$remesa=$this->cajero_model->llamados('RM');
			$data['remesa']=$remesa->idcliente;

		//echo $data['remesa'];
			$this->load->view('template/main',$data);
		}elseif($this->session->userdata('rol') == 'Recepcionista') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'ClienteCtrl';</script>";
		}else{
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Login_Controller/Logear';</script>";
		}
	}

	public function Transferencia($valor){
		if ($valor > 0) {
			$data=array('page_title'=>'Transferencia',
				'view'=>'transferencia',
				'data_view'=>array());

			$Estado=$this->cajero_model->estado_cliente();
			$data['Estado_c']=$Estado;

			$Transferencia=$this->cajero_model->transferecia();
			$data['Transferencia']=$Transferencia;

			$Estado=$this->cajero_model->Estado();
			$data['Estado']=$Estado;

			$clientes=$this->cajero_model->Obtener_Clientes($valor);
			$data['CL']=$clientes;

			$data['cajero']=$this->cajero_model->cambios($this->session->userdata('idusuario'),1);

			$this->load->view('template/main',$data);
		}else{
			echo "<script>alert('No hay clientes en espera');baseurl='".base_url()."';document.location.href=baseurl+'Cajero/cliente';</script>";
		}


	}

	public function transferecia_cliente(){
		if($this->input->is_ajax_request()){
			$data=array(
				'idcliente'=>$this->input->post('idcliente'),
				'fecha' => $this->input->post('fecha'),
				'idtransferencia' => $this->input->post('idtransferencia'),
				'codigo' => $this->input->post('codigo'),
				'idestado' => $this->input->post('idestado')
			);

			if($this->cajero_model->transferecia_cliente($data)){
				echo json_encode(array('success' => 1));
			}
			else
			{
				echo json_encode(array('success' => 0));
			}
		}
		else
		{
			echo "No se pudo acceder";
		}
	}

}

?>