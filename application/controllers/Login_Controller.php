<?php 
/**
 * 
 */
class Login_Controller extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Login_Model');
		$this->load->library(array('session'));
		$this->load->helper(array('url','form'));
	}

	public function Index()
	{
		$this->load->view('Login');
		if ($this->session->userdata('rol')=='Recepcionista') {
			redirect('ClienteCtrl/');
		}
		elseif ($this->session->userdata('rol')=='Administrador') {
			redirect('Rol_Controller/Listar');
		}
		elseif ($this->session->userdata('rol')=='Gerente') {
			redirect('Usuario/CargarUsuario');
		}
		elseif ($this->session->userdata('rol')=='Cajero') {
			redirect('Cajero/cliente');
		}
	}

	public function Logear()
	{
		$usuario = $this->input->post('usuario',true);
		$contrasena = base64_encode($this->input->post('contrasena',true));
		$validate = $this->Login_Model->Exist_User($usuario,$contrasena);

		if ($validate->num_rows() > 0) {
			$data = $validate->row_array();
			$idusuario = $data['idusuario'];
			$usuario = $data['usuario'];
			$contrasena = $data['contrasena'];
			$nombre = $data['nombre'];
			$id_rol = $data['id_rol'];
			$rol = $data['rol'];
			$escribir = $data['escribir'];
			$modificar = $data['modificar'];
			$eliminar = $data['eliminar'];

			$datos = array(
				'idusuario' => $idusuario,
				'usuario' => $usuario,
				'contrasena' => $contrasena,
				'nombre' => $nombre,
				'id_rol' => $id_rol,
				'rol' => $rol,
				'escribir' => $escribir,
				'modificar' => $modificar,
				'eliminar' => $eliminar,
				'is_logued_in' => TRUE
			);
			$this->session->set_userdata($datos);
			if ($this->session->userdata('rol')=='Recepcionista') {
			redirect('ClienteCtrl');
			}
			elseif ($this->session->userdata('rol')=='Administrador') {
				redirect('Rol_Controller/Listar');
			}
			elseif ($this->session->userdata('rol')=='Gerente') {
				redirect('Usuario/CargarUsuario');
			}
			elseif ($this->session->userdata('rol')=='Cajero') {
				redirect('Cajero/cliente');
			}
		}else{
			echo $this->session->set_flashdata('msg','Usuario o contraseña Incorrectos');
			redirect('Login_Controller');
		}
	}

	public function Logout()
	{
		$user_data = $this->session->all_userdata();
		foreach ($user_data as $key => $value) {
			if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
				$this->session->unset_userdata($key);
			}
		}
		$this->session->sess_destroy();
		redirect('Login_Controller');
	}



/* Image
	public function update_profile() { 
		$id = $this->session->userdata('id'); 
		$this->load->model('edit_profile_model'); 
		$config['upload_path'] = './uploads/'; 
		$config['allowed_types'] = 'gif|jpg|png'; 
		$config['max_size'] = '100'; 
		$config['max_width'] = '1024'; 
		$config['max_height'] = '768'; 
		$this->load->library('upload', $config); 
		$this->upload->do_upload();
	//upload the file to the above mentioned path $this->edit_profile_model->update_db_user_info($id, $this->upload->data());// pass the uploaded information to the model 
	} 


	 public function get_image($id){ 
	 	$this->db->where('id', $id); 
	 	$result = $this->db->get('userdetails'); 
	 	header("Content-type: image/jpeg"); echo $result['image']; 
	}
*/
}
?>