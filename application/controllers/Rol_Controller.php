<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 
 */

class Rol_Controller extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Rol_Model');
		$this->load->helper(array('url','form'));
	}

	public function Listar(){
		if ($this->session->userdata('rol') == 'Administrador') {
			$data = array(
				'page_title' => 'Rol',
				'view' => 'Listar_Rol',
				'data_view' => array());
			$roles = $this->Rol_Model->ReadRoles();
			$data['roles'] = $roles;
			$this->load->view('template/main',$data);
		}elseif($this->session->userdata('rol') == 'Recepcionista') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'ClienteCtrl';</script>";
		}elseif($this->session->userdata('rol') == 'Gerente') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Usuario/CargarUsuario';</script>";
		}elseif($this->session->userdata('rol') == 'Cajero') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Cajero/cliente';</script>";
		}else{
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Login_Controller/Logear';</script>";
		}
		
	}


	public function InsertNuevoRol()
	{
		if ($this->input->is_ajax_request()) {
			$data = array(
				'rol' => $this->input->post('rol'),
				'escribir'=> $this->input->Post('escribir'),
				'modificar'=> $this->input->Post('modificar'),
				'eliminar'=> $this->input->Post('eliminar'));
			if ($this->Rol_Model->InsertNuevoRol($data)) {
				echo json_encode(array('success'=>1));
			}else{
				echo json_encode(array('success'=>0));
			}
		}else{
			echo "Error fatal: No se puede acceder.....";
		}
	}

	public function EliminarRol()
	{
		$this->Rol_Model->EliminarRol();
		echo json_encode(array("status" => TRUE));
	}


	public function VistaActualizar($id)
	{
		if ($this->session->userdata('rol') == 'Administrador' || $this->session->userdata('rol')=='Gerente') {
			$data = array(
				'page_title' => 'Actualizar',
				'view' => 'act/Editar_Rol',
				'data_view' => array());
			$rol= $this->Rol_Model->ObtenerRol($id);
			$data['rol']= $rol;
			$this->load->view('template/main',$data);
		}elseif($this->session->userdata('rol') == 'Recepcionista') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'ClienteCtrl';</script>";
		}elseif($this->session->userdata('rol') == 'Cajero') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Cajero';</script>";
		}else{
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Login_Controller/Logear';</script>";
		}
	}

	public function ActualizarRol()
	{
		if ($this->input->is_ajax_request()) {
			$data = array(
				'id_rol' => $this->input->post('id_rol'),
				'rol' => $this->input->post('rol'),
				'escribir'=> $this->input->Post('escribir'),
				'modificar'=> $this->input->Post('modificar'),
				'eliminar'=> $this->input->Post('eliminar'));
			if ($this->Rol_Model->ActualizarRol($data)) {
				echo json_encode(array('success'=>1));
			}else{
				echo json_encode(array('success'=>0));
			}
		}else{
			echo "Error fatal: No se puede acceder.....";
		}
	}

}

?>