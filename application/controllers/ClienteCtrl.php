<?php 

defined('BASEPATH')OR exit('No direct script access allowed');

class ClienteCtrl extends CI_controller{

	public function __construct(){
		parent:: __construct();
		$this->load->model('ClienteMdl');
	}
	public function Index(){
		if ($this->session->userdata('rol')=='Recepcionista') {
			$data = array(
				'page_title' => 'Recepción',
				'view' => 'ClienteView',
				'data_view' => array());
			$transferencia = $this->ClienteMdl->transferencia();
			$data['transferencia'] = $transferencia;
			$this->load->view('template/main',$data);
		}else{
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Rol_Controller/Listar';</script>";
			
		}
	}



	public function GenerarCodigo(){
		$hora = $this->input->POST('hora');
		$transferencia= $this->input->POST('transferencia');

		$codigo = $this->ClienteMdl->GenerateCodigo($hora,$transferencia);
		//echo $codigo->{'codigo'};
		redirect('pdf_controller/generar_pdf');
			foreach ($codigo as $f) {
				echo '<script type="text/javascript">alert("Codigo Generado '.$f->codigo.'");</script>';
			}
			
		}
	}

?>