<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller
{
	
	public function __construct()
	{
		parent:: __construct();

		$this->load->model('UsuarioMdl');
		$this->load->helper('url');
	}
	public function CargarUsuario()
	{
		if ($this->session->userdata('rol') == 'Administrador' || $this->session->userdata('rol')=='Gerente') {
			$data = array(
				'page_title' => 'USUARIOS',
				'view' => 'usuario',
				'data_view' => array()
			);
			$rol = $this->UsuarioMdl->DatosRol();
			$data['rol'] = $rol;
			$usuario = $this->UsuarioMdl->UsuarioRol();
			$data['usuario'] = $usuario;
			$this->load->view('template/main',$data);
		}elseif($this->session->userdata('rol') == 'Recepcionista') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'ClienteCtrl';</script>";
		}elseif($this->session->userdata('rol') == 'Cajero') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Cajero/cliente';</script>";
		}else{
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Login_Controller/Logear';</script>";
		}
	}
	public function FormUsuario()
	{
		if ($this->session->userdata('rol') == 'Administrador') {
			$data = array(
				'page_title' => 'FORMULARIO DE USUARIOS',
				'view' => 'form/usuario',
				'data_view' => array()
			);
			$rol = $this->UsuarioMdl->DatosRol();
			$data['rol'] = $rol;
			$this->load->view('Template/main',$data);
		}elseif($this->session->userdata('rol') == 'Recepcionista') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'ClienteCtrl';</script>";
		}elseif($this->session->userdata('rol') == 'Gerente') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Usuario/CargarUsuario';</script>";
		}elseif($this->session->userdata('rol') == 'Cajero') {
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Cajero/cliente';</script>";
		}else{
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Login_Controller/Logear';</script>";
		}
	}
	public function InsertarUsuario(){

		if($this->input->is_ajax_request())
		{
			$data = array(
				'usuario' => $this->input->post('usuario'),
				'contrasena' => base64_encode($this->input->post('contrasena')),
				'nombre' => $this->input->post('nombre'),
				'id_rol' => $this->input->post('id_rol')
			);
			if($this->UsuarioMdl->AgregarUsuario($data))
			{
				echo json_encode(array('success' =>1));
			}
			else
			{
				echo json_encode(array('success' =>0));
			}
		}
		else
		{
			echo "No se puede acceder";
		}
	}
	public function ActUsuario($idusuario)
	{
		if ($this->session->userdata('rol') == 'Administrador') {
			$data = array(
				'page_title' => 'ACTUALIZADOR DE USUARIOS',
				'view' => 'act/usuario',
				'data_view' => array()
			);
			$rol = $this->UsuarioMdl->DatosRol();
			$data['rol'] = $rol;
			$usuario = $this->UsuarioMdl->ObtenerDatos($idusuario);
			$data['mate'] = $usuario;
			$this->load->view('Template/main',$data);
		}else{
			echo "<script>var baseurl = '".base_url()."';alert('Acceso denegado');document.location.href=baseurl+'Rol_Controller/Listar';</script>";
		}
	}
	public function UpdateUsuario()
	{
		if($this->input->is_ajax_request())
		{
			$data = array(
				'idusuario' => $this->input->post('idusuario'),
				'usuario' => $this->input->post('usuario'),
				'contrasena' => base64_encode($this->input->post('contrasena')),
				'nombre' => $this->input->post('nombre'),
				'id_rol' => $this->input->post('id_rol')
			);
			if($this->UsuarioMdl->ActualizarUsuario($data))
			{
				echo json_encode(array('success' =>1));
			}
			else
			{
				echo json_encode(array('success' =>0));
			}
		}
		else
		{
			echo "No se puede acceder";
		}
	}
	public function DeleteUsuario()
	{
		$this->UsuarioMdl->EliminarUsuario();
		echo json_encode(array("status" => TRUE));
	}
}
?>