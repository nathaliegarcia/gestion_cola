<?php defined('BASEPATH') OR exit('No direct script access allowed');

class pdf_controller extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('pdf_model');
    $this->load->model('ClienteMdl');
  } 

    public function Index(){
    if ($this->session->userdata('rol')=='Recepcionista') {
      $data = array(
        'page_title' => 'Recepción',
        'view' => 'ClienteView',
        'data_view' => array());
      $transferencia = $this->ClienteMdl->transferencia();
      $data['transferencia'] = $transferencia;
      $this->load->view('template/main',$data);
    }else{
      echo "<script>var baseurl = 'http://localhost/gestion_cola/';alert('Acceso denegado');document.location.href=baseurl+'Rol_Controller/Listar';</script>";
      
    }
  }


  public function generar_pdf(){
    $pdf = $this->pdf_model->get_codigo_pdf();
    $data['codigo'] = $pdf;
    $this->load->view('CodigoView',$data);
        // Get output html
    $html = $this->output->get_output();

        // Load pdf library
    
        // Load HTML content
    $this->dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation
    $this->dompdf->setPaper('A9', 'portrait');

        // Render the HTML as PDF
    $this->dompdf->render();

        // Output the generated PDF (1 = download and 0 = preview)
    $this->dompdf->stream("Codigo.pdf", array("Attachment=>0"));

  }

}
?>