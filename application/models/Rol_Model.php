<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Rol_Model extends CI_Model
{
	public function ReadRoles(){
		$query = $this->db->get('roles');
		return $query->result();
	}


	public function InsertNuevoRol($data)
	{
		return($this->db->Insert('roles',$data)) ?true:false;
	}

	public function EliminarRol()
	{
		$id = $this->input->post('rol_id');
        $this->db->where('id_rol', $id);
        $this->db->delete('roles');
	}

	public function ObtenerRol($id)
	{
		$this->db->select('*');
		$this->db->from('roles');
		$this->db->where('id_rol',$id);
		$query=$this->db->get();
		return $query->row();
	}
	public function ActualizarRol($data)
	{
		$this->db->where('id_rol',$data['id_rol']);
		return($this->db->Update('roles',$data)) ?true:false;
	}
}
?>