<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class UsuarioMdl extends CI_Model
{
	
	public function DatosUsuario()
	{
		$usuario = $this->db->get('usuarios');
		return $usuario->Result();
	}
	public function DatosRol()
	{
		$rol = $this->db->get('roles');
		return $rol->Result();
	}
	public function UsuarioRol()
	{
		$this->db->select('u.idusuario, u.usuario, u.nombre, r.rol');
		$this->db->from('usuarios u');
		$this->db->join('roles r', 'u.id_rol = r.id_rol');
		$usuario = $this->db->get();
		return $usuario->Result();
	}
	public function AgregarUsuario($data)
	{
		return($this->db->insert('usuarios',$data)) ? true:false;
	}
	public function ObtenerDatos($idusuario)
	{
		$this->db->select('*');
		$this->db->from('usuarios');
		$this->db->where('idusuario = '. $idusuario);
		$idusuario = $this->db->get();
		return $idusuario->Row();
	}
	public function ActualizarUsuario($data)
	{
		$this->db->where('idusuario', $data['idusuario']);
		return($this->db->update('usuarios',$data)) ? true:false;
	}
	public function EliminarUsuario()
	{
		$id = $this->input->post('user_id');
        $this->db->where('idusuario', $id);
        $this->db->delete('usuarios');
	}
}
 ?>