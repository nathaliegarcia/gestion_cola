<?php defined('BASEPATH') OR exit('No direct script access allowed');


class cajero_model extends CI_model 
{
	public function cajero(){
		$this->db->select('C.Id_Cajero, c.N_caja, E.estado');
		$this->db->from('cajero as C');
		$this->db->join('estado as E', 'E.idestado = C.idestado','left');	
		$cajero=$this->db->get();
		return $cajero->Result();
	}

	public function Agregar_Cajero($data){
		return ($this->db->insert('cajero',$data)) ? true:false;
	}

	public function Estado(){   
		$Estado=$this->db->get('estado'); 
		return $Estado->Result(); 
	}

	public function estado_cliente(){   
		$Estado=$this->db->get('estado_cliente'); 
		return $Estado->Result(); 
	}	

	public function Obtener_Cajero($Id_Cajero){
		$this->db->select('*');
		$this->db->from('cajero');
		$this->db->where('Id_Cajero ='.$Id_Cajero);
		$Id_Cajero = $this->db->get();
		return ($Id_Cajero->num_rows() ===1) ? $Id_Cajero->row(): false;
	}

	public function Actualizar_Cajero($data){
		$this->db->where('Id_Cajero', $data['Id_Cajero']);
		return ($this->db->update('cajero',$data)) ? true:false;
	}
	/*public function eliminar($Id_Cajero){
		$this->db->where('Id_Cajero', $Id_Cajero);
		return ($this->db->delete('cajero')) ? true: false;
	}*/

	public function Eliminar(){
		$id=$this->input->post('cajero_Id');
		$this->db->where('Id_Cajero',$id);
		$this->db->delete('cajero');
	}

	/*public function clientes(){   
		$cliente=$this->db->get('cliente'); 
		return $cliente->Result(); 
	}*/

//Transferencia//
	public function transferecia(){   
		$transferecia=$this->db->get('transferencias'); 
		return $transferecia->Result(); 
	}

	public function Obtener_Clientes($idcliente){
		$this->db->set('idestado',2);
		$this->db->where('idcliente ='. $idcliente);
		$this->db->update('cliente');
		$this->db->select('*');
		$this->db->from('cliente');
		$this->db->where('idcliente ='. $idcliente);
		$idcliente = $this->db->get();
		return ($idcliente->num_rows()=== 1) ? $idcliente->row(): false;
	}
	public function cliente(){  
		$cliente=$this->db->get('cliente'); 
		return $cliente->Result(); 
	} 

	public function llamados($op){
		$this->db->select_min('idcliente');
		$this->db->from('cliente');
		$this->db->where('idestado =', 1);
		$this->db->like('codigo',$op);
		$idcliente =$this->db->get();
		//$idcliente = $this->db->get('cliente');
		return ($idcliente->num_rows()===1) ? $idcliente->row(): false;
	}

	public function transferecia_cliente($data){
		$this->db->where('idcliente', $data['idcliente']);
		return ($this->db->update('cliente',$data)) ? true:false;
	}

	public function cambios($idCajero,$idEstado){
		$this->db->set('idestado',$idEstado);
		$this->db->where('Id_Cajero', $idCajero);
		return ($this->db->update('cajero')) ? true:false;
	}

}

?>