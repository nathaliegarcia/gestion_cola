<style type="text/css">
	#btn1{
		width: 160px;
	}
</style>
<body>
	<div align="center">
		<br>
		<?php if ($abono > 0): ?>
			<a class="waves-effect waves-light btn-large btn pink darken-4 white-text" href="<?php echo base_url().'Cajero/Transferencia/'.$abono?>"><i class="material-icons left">attach_money</i>ABONOS</a>
			<?php else: ?>
				<a class="waves-effect waves-light btn-large btn pink darken-4 white-text" href="<?php echo base_url().'Cajero/Transferencia/'.$abono=0?>"><i class="material-icons left">attach_money</i>ABONOS</a>
			<?php endif; ?>

			<?php if ($prestamo > 0): ?>
				<a class="waves-effect waves-light btn-large btn orange darken-4 white-text" href="<?php echo base_url().'Cajero/Transferencia/'.$prestamo?>"><i class="material-icons left">attach_money</i>PRESTAMO</a>
				<?php else: ?>
					<a class="waves-effect waves-light btn-large btn orange darken-4 white-text" href="<?php echo base_url().'Cajero/Transferencia/'.$prestamo=0?>"><i class="material-icons left">attach_money</i>PRESTAMO</a>
				<?php endif; ?>

				<?php if ($retiro > 0): ?>
					<a class="waves-effect waves-light btn-large btn blue darken-4 white-text" href="<?php echo base_url().'Cajero/Transferencia/'.$retiro?>"><i class="material-icons left">attach_money</i>RETIRO</a>
					<?php else: ?>
						<a class="waves-effect waves-light btn-large btn blue darken-4 white-text" href="<?php echo base_url().'Cajero/Transferencia/'.$retiro=0?>"><i class="material-icons left">attach_money</i>RETIRO</a>
					<?php endif; ?>


					<?php if ($remesa > 0): ?>
						<a class="waves-effect waves-light btn-large btn purple darken-4 white-text" href="<?php echo base_url().'Cajero/Transferencia/'.$remesa?>"><i class="material-icons left">attach_money</i>REMESA</a>
						<?php else: ?>
							<a class="waves-effect waves-light btn-large btn purple darken-4 white-text" href="<?php echo base_url().'Cajero/Transferencia/'.$remesa= 0?>"><i class="material-icons left">attach_money</i>REMESA</a>
						<?php endif; ?>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Cajero.js"></script>
	</div>
</body>
</html>