<style type="text/css">
	#btn1{
		width: 160px;
	}
</style>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.4.1.min.js"></script>
</head>
<body>
	<div class="container" align="center">
		<form>
			<button class="waves-effect waves-light btn-large btn blue darken-4 white-text" onclick="clientes()" style="float: right;">LLAMADO<i class="material-icons left">account_circle</i></button>
		</form>
		<div class="card" style="width: 450px; border-color: black">
			<div class="card-body" style="background-color:">
				<h5 class="card-title"></h5>
				<div class="form-group">			
					<div class="col-md-12">
						<label>Transferecia</label>
						<select id="Transferecia" class="browser-default text text-dark" disabled="">
							<option>Transferencias</option>
							<?php foreach ($Transferencia as $valor): ?>
								<option value="<?=$valor->idtransferencia?>"<?=$valor->idtransferencia == $CL->idtransferencia? 'selected': ''?>>
									<?=$valor->transferencia?></option>
								<?php endforeach ?>
							</select>
						</div>
						<div class="col-md-12">
							<label>Codigo</label>
							<input type="text" id="Codigo" readonly value="<?=$CL->codigo?>">
						</div>
						<input type="hidden" id="fecha" value="<?=$CL->fecha?>">
						<div class="col-md-12">
							<label>Estado cliente</label>
							<select id="Estado" class="browser-default">
								<option>Estado del cliente</option>
								<?php foreach ($Estado_c as $valor): ?>
									<option value="<?=$valor->idestado?>"<?=$valor->idestado == $CL->idestado? 'selected': ''?>>
										<?=$valor->estado?></option>
									<?php endforeach ?>
								</select>
								<input type="hidden" id="idcliente" value="<?=$CL->idcliente?>">
								<br>
								<br>
								<button class="btn waves-effect waves-light" type="button" id="libre">Cambiar<i class="material-icons right">send</i>
								</button>
								<a href="<?php echo base_url(); ?>Cajero/cliente" class="btn waves-effect waves-light"><i class="material-icons left">clear</i>Regresar</a>
								<br><br>

								<script type="text/javascript">
									function clientes() {

										var Cliente = prompt("Cliente siguiente es","<?=$CL->codigo?>");

										if (Cliente != null){
											alert("Cliente" + Cliente);
										}
										else {
											alert("No has llamado un Cliente");
										}
									}
								</script>
							</div>
						</div>
					</div>
				</div>
			</div>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
			<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Cajero.js"></script>
		</body>
		</html>