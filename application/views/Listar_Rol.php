<style type="text/css">
	#box12{
		width: 900px;
	}
</style>
<body class="bg teal darken-3">
	<center>
		<div class="container" id="box12">
			<h5 class="white-text"><b>Roles y permisos</b></h5>
			
			<!-- Modal Btn -->
			<?php if ($this->session->userdata('escribir')==='si'): ?>
				<button data-target="modal1" style="float: right;" class="btn modal-trigger waves-effect waves-light btn blue darken-3"><i class="material-icons left">add</i>Nuevo</button>
				<?php else: ?>
					<button disabled  data-target="modal1" style="float: right;" class="btn modal-trigger waves-effect waves-light btn orange darken-4"><i class="material-icons left">add</i>Nuevo</button>
				<?php endif ?>
				
				<br><br><br>
				<table class="table centered table-bordered table-hover table-dark col-md-8 center-text z-depth-4">
					<thead>
						<tr class="teal lighten-2">
							<th>Rol</th>
							<th style="width: 5px;">Escribir</th>
							<th style="width: 5px;">Modificar</th>
							<th style="width: 5px;">Eliminar</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($roles as $R): ?>
							<tr id="id_rol<?php echo $R->id_rol; ?>">
								<td><?=$R->rol ;?></td>
								<td><?=$R->escribir ;?></td>
								<td><?=$R->modificar ;?></td>
								<td><?=$R->eliminar ;?></td>
								<td>

									<?php if ($this->session->userdata('modificar')==='si'): ?>
										<a href="<?php echo base_url(); ?>Rol_Controller/VistaActualizar/<?=$R->id_rol ;?>" class="btn waves-effect waves-light btn blue darken-4 white-text"><i class="large material-icons">create</i></a>
										<?php else: ?>
											<a disabled class="waves-effect waves-light btn cyan accent-3"><i class="large material-icons">create</i></a>
										<?php endif ?>
										<?php if ($this->session->userdata('eliminar')==='si'): ?>
											<a href="javascript:void(0)" id="delet" data-id="<?php echo $R->id_rol;?>" class="waves-effect waves-light btn red darken-4 white-text"><i class="large material-icons">delete_forever</i></a>
											<?php else: ?>
												<a disabled class="waves-effect waves-light btn red darken-4 white-text"><i class="large material-icons">delete_forever</i></a>
											<?php endif ?>
										</td>
									</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						<!-- Modal Window -->
						<div id="modal1" class="modal">
							<div class="modal-content">
								<?php $this->load->view('form/Agregar_rol') ?>
							</div>
						</div>

					</div>
				</center>
			</body>
			<script>
				var SITEURL = '<?php echo base_url();?>';
				$(document).ready(function(){

					$('body').on('click', '#delet', function () {

						var rol_id = $(this).data("id");
						if (confirm("Are you sure delete this Rol?")) {
							$.ajax({
								type: "Post",
								url: SITEURL + "Rol_Controller/EliminarRol",
								data: {
									rol_id: rol_id
								},
								dataType: "json",
								success: function (data) {
									$("#id_rol" + rol_id).remove();

								},
								error: function (data) {
									console.log('Error:', data);
								}
							});
						}
					});
				});
			</script>