<style type="text/css">
	.modal {
		transform: scale(0, 0);
		transition: transform .3s ease;
	}
</style>
<body>
	<center>
		<h4><B>AGREGAR CAJERO</B></h4>
		<div class="form-row">
			<div class="col-md-12">
				<input type="number" id="Numero" class="form-control" required="" placeholder="Ingrese Su Numero de caja"><br>
			</div>
			<div class="col-md-12">
				<select id="Estado" class="browser-default">
					<option>Agregue su estado</option>
					<?php foreach ($Estado as $valor): ?>
						<option value="<?php echo $valor->idestado ;?>"><?php echo $valor->estado; ?></option>
					<?php endforeach ?>
				</select>
				<br>
			</div>
		</div>
		<div class="form-row">
			<div class="col-md-6">
				<button class="btn blue darken-4 waves-effect waves-light" id="Agregar" style="float: left;">AGREGAR CAJERO
					<i class="material-icons left">sd_card</i>
				</button>
			</div>
			<div class="col-md-6">
				<a href="<?php echo base_url(); ?>Cajero" class="btn btn blue darken-4 waves-effect waves-light white-text" style="float: right;" ><i class="material-icons left">clear</i>Cancelar</a>
			</div>
		</div>
	</center>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Cajero.js"></script>
</body>