<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body class="teal darken-3">
	<form id="genera" method="POST" class="user" action="<?php echo base_url().'ClienteCtrl/GenerarCodigo';?>" >
		<center>
			<div style="position: relative; bottom: -100px;" class=" my-5 badge teal darken-3">
				<h2 class="text-white"><b>Gestión De Colas</b></h2>
				<br>
				<div class="select-field">
				<select required="" class="form-control browser-default" name="transferencia">
					<option value="">Seleccionar Transferencia</option>
					<?php foreach ($transferencia as $valor): ?>
						<option value="<?=$valor->idtransferencia ?>"><?=$valor->transferencia ?></option>
					<?php endforeach; ?>                
				</select>
				<br>
				<input type="hidden" id="hora" name="hora"  class="form-control" value="<?php echo date('y-m-d'); ?>" />
				<br>
				<button id="genera" class="btn green" ><i class="large material-icons left">assignment_turned_in</i>Generar</button>
			</div>
			<script>
				$(document).ready(function(){
					$("#genera").submit(function(){
						alert("Codigo Generado");

					});
				});
			</script>
		</center>
	</form>
</body>
</html>