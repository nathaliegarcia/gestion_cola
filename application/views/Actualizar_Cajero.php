<body style="background-color:">
	<div align="center">
		<div align="center">
			<div class="card col-lg-4">
				<div class="card-body" style="background-color:">
					<h5 class="card-title">
						<b>ACTUALIZAR CAJERO</b>
					</h5>
					<div class="form-group">		
						<label>Numero de caja</label><br>
						<input type="text" id="Numero" class="form-control" value="<?=$C->N_caja?>"><br>
						<label>Estado</label><br>
						<select id="Estado" class="browser-default">
							<option>Actualice su estado</option>
							<?php foreach ($Estado as $valor): ?>
								<option value="<?=$valor->idestado?>"<?=$valor->idestado == $C->idestado? 'selected': ''?>>
									<?=$valor->estado?></option>
								<?php endforeach ?>
							</select><br><br>	
							<input type="hidden" id="Id_Cajero" value="<?=$C->Id_Cajero?>">
						</div>
						<div class="form-row">
							<div class="col-lg-6">
								<button class="btn blue darken-4 waves-effect waves-light" id="Actualizar">Guardar<i class="material-icons left">sd_card</i></button>
							</div><br>
							<div class="col-lg-6">
								<a href="<?php echo base_url(); ?>Cajero" class="btn btn blue darken-4 waves-effect waves-light white-text" style="float: right;" ><i class="material-icons left">clear</i>Cancelar</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/Cajero.js"></script>
	</body>