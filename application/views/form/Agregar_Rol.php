<style type="text/css">
	body{
		text-shadow: 10px;
		font-weight: bold;
	}
	.modal {
		transform: scale(0, 0);
		transition: transform .2s ease;
	}
</style>
<body>
	<div class="card-title" style="color: #0d47a1;"><h5 class="text-blue darken-4">Nuevo rol</h5></div>
	<div class="card-body" style="color: #0d47a1;">
		<div class="card">
			<div class="col-md-12">
				<div class="input-field col s6">
					<input type="text" id="rol" placeholder="Escribir nombre del nuevo rol">
					<label>Rol:</label>
				</div>
			</div>
		</div>
		<div class="card">
			<div  class="card-title">
				<h5>Permisos</h5>
			</div>
			<div class="card-body">
				<div class="form-row">
					<div class="col-md-4">
						<div class="input-field col s12">
							<select id="escribir">
								<option>--Asignar--</option>
								<option value="si">Si</option>
								<option value="no">No</option>
							</select>
							<label>Escribir</label>
						</div>

					</div>

					<div class="col-md-4">
						<div class="input-field col s12">
							<select id="modificar">
								<option>--Asignar--</option>
								<option value="si">Si</option>
								<option value="no">No</option>
							</select>
							<label>Modificar</label>
						</div>
					</div>

					<div class="col-md-4">
						<div class="input-field col s12">
							<select id="eliminar" >
								<option>--Asignar--</option>
								<option value="si">Si</option>
								<option value="no">No</option>
							</select>
							<label>Eliminar</label>
						</div>
					</div>

				</div><br>
			</div>
		</div>
	</div>
	
	<div class="card-foot">
		<div class="col-md-12">
			<button id="NewRol" class="waves-effect waves-light btn blue darken-4"><i class="material-icons left">sd_card</i>Guardar</button>
			<a class="waves-effect waves-light btn blue darken-4 white-text" href="<?php echo  base_url(); ?>Rol_Controller/Listar"><i class="material-icons left">close</i>Cancelar</a><br><br>
		</div>
	</div>
</body>