<style type="text/css">
	.modal {
    transform: scale(0, 0);
    transition: transform .3s ease;
}
</style>
<body>
	<div>
		<div>
			<h4 class="card-head black-text"><b>Nuevo usuario</b></h4>
		</div>
		<div class="form-row">
			<div class="col-md-12">
				<label><b>Usuario</b></label>
				<input type="text" id="usuario" placeholder="Ejemplo: Richar_Rmiarez">
			</div>
			<div class="col-md-12">
				<label><b>Nombre</b></label>
				<input type="text" id="nombre" placeholder="Ejemplo: Jonh Rmiarez">
			</div>
			<div class="col-md-12">
				<label><b>Contraseña</b></label>
				<input type="text" id="contrasena" placeholder="Ejemplo: Aa1234">
			</div>
			<div class="col-md-12">
				<label><b>Rol</b></label>
				<select id="id_rol" class="browser-default">
					<option value="">--Elejir Rol--</option>
					<?php foreach($rol as $R):?>
						<option value="<?=$R->id_rol?>"><?=$R->rol?></option>
						<?php endforeach;?>
				</select>
			</div>
			<div class="col-md-12" style="margin-top: 10px">
				<button class="btn blue darken-4 waves-effect waves-light" id="agregarusuario"><i class="material-icons left">sd_card</i>Guardar</button>
				<a class="btn blue darken-4 white-text waves-effect waves-light" href="<?php echo base_url()?>Usuario/CargarUsuario"><i class="material-icons left">clear</i>Cancelar</a>
			<br></div>
		</div>
	</div>
	<script type="text/javascript" src="<?php echo base_url().'assets/js/usuario.js';?>"></script>
</body>