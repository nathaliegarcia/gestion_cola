<style type="text/css">
	#paginado{
		margin: 10px;
		padding: 10px;
	}
	#btn1,#btn2{
		width: 150px;
	}
</style>
<body class="teal darken-3">
	<center>

		<div align="center" class="container">
			<h5 class="white-text"><b>Cajero</b></h5>
			<div class="form-row" style="float: right;">
				<pre>
					<a href="<?php echo base_url(); ?>Cajero/cliente" style="float: right;" id="btn1" class="btn btn green darken-3 white-text waves-effect waves-light" >Cliente<i class="material-icons left">people</i></a>

					<?php if ($this->session->userdata('modificar')==='si') { ?>
						<button class="btn blue darken-3 waves-effect waves-light btn modal-trigger" id="btn2" href="#modal1" >Nuevo<i class="material-icons left">add</i></button>
					<?php }else{ ?>
						<button class="btn blue darken-3 waves-effect waves-light btn modal-trigger" id="btn2" href="#modal1" >Nuevo<i class="material-icons left">add</i></button>
					<?php } ?>
				</pre>			
			</div>
			<br><br>
			<div align="center">
				<div id="modal1" class="modal" style="height: 300px; width: 600px; background-color: white;">
					<div class="modal-content" style="width: 600px; background-color: white;">
						<?php $this->load->view('Agregar_Cajero')?>
					</div>
				</div> 
			</div>

			<div id="paginado">
				<table class="centered table-bordered table-hover table-dark col-lg-8 col-md-8 center-text z-depth-4">
					<thead class="teal lighten-2">
						<th>Estado</th>
						<th>N°caja</th>
						<th>Acciones</th>
					</thead>
					<tbody>
						<?php foreach ($Cajero as $valor): ?>
							<tr id="cajero<?php echo $valor->Id_Cajero;?>" >
								<td><?=$valor->estado?></td>
								<td>Caja N°<?=$valor->N_caja?></td>
								<td>
									<?php if ($this->session->userdata('modificar')==='si'): ?>
										<a href="<?php echo base_url().'Cajero/Update_Cajero/'. $valor->Id_Cajero?>" class="btn blue darken-4 waves-effect waves-light white-text"><i class="large material-icons">create</i></a>
										<?php else: ?>
											<a disabled class="btn blue darken-4 waves-effect waves-light white-text"><i class="large material-icons">create</i></a>
										<?php endif; ?>
										<?php if ($this->session->userdata('eliminar')==='si'): ?>
											<a href="javascript:void(0)" id="eliminar" data-id="<?php echo $valor->Id_Cajero?>" class="btn red darken-4 waves-effect waves-light white-text"><i class="large material-icons">delete_forever</i></a>
											<?php else: ?>
												<a disabled href="<?php echo base_url().'Cajero/Eliminar/'. $valor->Id_Cajero ?>" class="btn red darken-4 waves-effect waves-light white-text"><i class="large material-icons">delete_forever</i></a>
											<?php endif; ?>

										</td>
									</tr>
								</tbody>
							<?php endforeach; ?>
						</table>
					</div>
				</div>
			</center>
			<script>
				var Caja = '<?php echo base_url(); ?>';
				$(document).ready(function(){
					$('body').on('click', '#eliminar' , function (){
						var cajero_Id = $(this).data("id");
						if(confirm("¿Seguro? que quieres eliminar este cajero")){
							$.ajax({
								type: "post",
								url: Caja + "Cajero/Eliminar",
								data:{
									cajero_Id: cajero_Id
								},
								dataType: "json",
								success: function (data){
									$('#cajero' + cajero_Id).remove();
								},
								error: function (data){
									console.log('Error:', $data);
								}
							});
						}

					});
				});
			</script>
		</body>
		</html>