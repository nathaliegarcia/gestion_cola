<body style="color: white">
	<center>
		<div class="card col-5 grey darken-3">
			<div class="card-header teal lighten-2">
				<h4 style="font-family: Times roman;"><b>Editar usuario</b></h4>
			</div>
			<div class="form-row">
				<div class="col-md-12">
					<b>Usuario</b>
					<input type="text" id="usuario" class=" form-control" value="<?=$mate->usuario?>" style="color: white">
				</div>
				<div class="col-md-12">
					<b>Nombre</b>
					<input type="text" id="nombre" class=" form-control" value="<?=$mate->nombre?>" style="color: white">
				</div>
				<div class="col-md-12">
					<b>Contraseña</b>
					<input type="text" id="contrasena" class=" form-control" value="<?php echo base64_decode($mate->contrasena)?>"style="color: white">
				</div>
				<div class="col-md-12">
					<b>Rol</b>
					<select id="id_rol">
						<option value="">--Elejir Rol--</option>
						<?php foreach($rol as $R):?>
							<option value="<?=$R->id_rol?>"<?=$R->id_rol == $mate->id_rol? 'selected': ''?>>
								<?=$R->rol?></option>
						<?php endforeach;?>
					</select>
				</div>
				<div class="col-md-12" style="margin-top: 10px">
					<input type="hidden" id="idusuario" value="<?=$mate->idusuario?>">
					<button class="btn blue darken-4 white-text waves-effect waves-light" id="actusuario">Guardar</button>
					<a class="btn blue darken-4 white-text waves-effect waves-light" href="<?php echo base_url()?>Usuario/CargarUsuario">Regresar</a>
				<br></div>
			</div>
		</div>
	</center>
	<script type="text/javascript" src="<?php echo base_url().'assets/js/usuario.js';?>"></script>
</body>