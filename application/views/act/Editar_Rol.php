<style type="text/css">
	body{
		text-shadow: 10px;
		font-weight: bold;
	}
</style>
<body>
	<center>
		<div class="container">
			<div class="card col-md-7" style="color: #0d47a1;">
				<div class="card-title"><h5>Editar rol</h5></div>
				<!-- Cuerpo -->
				<div class="card-body">
					<div class="card">
						<div class="col-md-12">
							<div class="input-field col s6">
								<input type="hidden" id="id_rol" value="<?=$rol->id_rol ;?>">
								<input type="text" id="rol" class="form-control" placeholder="Escribir nombre del nuevo rol" value="<?=$rol->rol ;?>">
								<label>Rol:</label>
							</div>
						</div>
					</div>
					<div class="card">
						<div  class="card-title">
							<h5>Permisos</h5>
						</div>
						<div class="card-body">
							<div class="form-row">
								<div class="col-md-4">
									<div class="input-field col s12">
										<select id="escribir" >
											<?php if ($rol->escribir === 'si'): ?>
												<option value="si">Si</option>
												<option value="no">No</option>
												<?php else: ?>
													<option value="no">No</option>
													<option value="si">Si</option>
												<?php endif; ?>
										</select>
										<label>Escribir</label>
									</div>
								</div>

								<div class="col-md-4">
									<div class="input-field col s12">
										<select id="modificar" >
											<?php if ($rol->modificar === 'si'): ?>
												<option value="si">Si</option>
												<option value="no">No</option>
												<?php else: ?>
													<option value="no">No</option>
													<option value="si">Si</option>
												<?php endif; ?>
											</select>
											<label>Modificar</label>
									</div>
								</div>

								<div class="col-md-4">
									<div class="input-field col s12">
										<select id="eliminar" >
											<?php if ($rol->eliminar === 'si'): ?>
												<option value="si">Si</option>
												<option value="no">No</option>
												<?php else: ?>
													<option value="no">No</option>
													<option value="si">Si</option>
											<?php endif; ?>
										</select>
										<label>Eliminar</label>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card-foot">
					<div class="col-md-12">
						<button id="UpdateRol" class="waves-effect waves-light btn blue darken-4"><i class="material-icons left">sd_card</i>Guardar</button>
						<a href="<?php echo base_url(); ?>Rol_Controller/Listar" class="waves-effect waves-light btn blue darken-4 white-text"><i class="material-icons left">clear</i>Cancelar</a><br><br>
					</div>
				</div>
			</div>
		</div>
	</center>
</body>