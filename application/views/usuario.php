<body style="color: white">
	<center>
		<div class="container">
			<h5 class="white-text"><b>Usuarios</b></h5>
			<div class="col-md-12">
				<?php if ($this->session->userdata('escribir')==='si'): ?>
					<button style="float: right;" data-target="modal1"  class="btn modal-trigger waves-effect waves-light btn blue darken-3"><i class="material-icons left">add</i>Nuevo</button>
					<?php else: ?>
						<button style="float: right;" disabled data-target="modal1"  class="btn modal-trigger waves-effect waves-light btn blue darken-4"><i class="material-icons left">add</i>Nuevo</button>
				<?php endif; ?>
			</div><br><br><br>
				<div class="col-lg-12">
					<table class="centered table-bordered table-hover table-dark col-lg-8 center-text z-depth-4" id="myTable">
						<thead class="teal lighten-2 center-align white-text">
							<th>Usuario</th>
							<th>Nombre</th>
							<th>Rol</th>
							<th>Acciones</th>
						</thead>
						<tbody>
							<?php foreach($usuario as $U): ?>
								<tr id="id_user<?php echo $U->idusuario;?>">
									<td><b><?=$U->usuario?></b></td>
									<td><b><?=$U->nombre?></b></td>
									<td><b><?=$U->rol?></b></td>
									<td>
										<?php if ($this->session->userdata('modificar')==='si') :?>
											<a href="<?php echo base_url().'Usuario/ActUsuario/'.$U->idusuario?>" class="btn waves-effect btn blue darken-4 white-text"><i class="large material-icons">create</i></a>
											<?php else: ?>
												<a disabled href="<?php echo base_url().'Usuario/ActUsuario/'.$U->idusuario?>" class="btn waves-effect btn blue darken-4 white-text"><i class="large material-icons">create</i></a>
											<?php endif; ?>
											<?php if ($this->session->userdata('eliminar')==='si') { ?>
												<a href="javascript:void(0)" id="delet" data-id="<?php echo $U->idusuario;?>" class="btn waves-effect red darken-4 white-text"><i class="large material-icons">delete_forever</i></a>
											<?php }else{ ?>
												<a disabled href="<?php echo base_url().'Usuario/DeleteUsuario/'.$U->idusuario?>" class="btn waves-effect red darken-4 white-text"><i class="large material-icons">delete_forever</i></a>
										<?php } ?>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
				</div>
		</div>
		<div id="modal1" class="modal" style="height: 480px; width: 490px;">
			<div class="modal-content" style="width: 500px;">
				<?php $this->load->view('form/usuario')?>
			</div> 
		</div>
	</center>
	<script>
		var SITEURL = '<?php echo base_url();?>';
		$(document).ready(function(){

			$('body').on('click', '#delet', function () {

				var user_id = $(this).data("id");
				if (confirm("Estas seguro de eliminar este usuario !")) {
					$.ajax({
						type: "Post",
						url: SITEURL + "Usuario/DeleteUsuario",
						data: {
							user_id: user_id
						},
						dataType: "json",
						success: function (data) {
							$("#id_user" + user_id).remove();
						},
						error: function (data) {
							console.log('Error:', data);
						}
					});
				}
			});
		});
	</script>
</body>