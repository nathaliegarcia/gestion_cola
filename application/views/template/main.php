<!DOCTYPE html>
<html>
<head>
	<title><?=$page_title?></title>
	 <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/Captura.PNG">
</head>
<body class="teal darken-3">
	<?php $this->load->view('template/header')?>
	<?php $this->load->view($view,$data_view)?>
	<?php $this->load->view('template/footer')?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/rol.js"></script>
<script type="text/javascript">
    if(history.forward(1)){
        location.replace(history.forward(1));
    }
</script>
</body>
