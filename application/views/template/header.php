<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php if ($this->session->userdata('rol')=='Administrador' || $this->session->userdata('rol')=='Gerente'): ?>
	
	<nav class="navbar navbar-expand-lg grey darken-4">
		<a class="navbar-brand"><img style="height: 175px;" src="<?php echo base_url()?>assets/t2.png"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="container-fluid" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link white-text" href="<?php echo base_url();?>Cajero"><b>Cajeros</b></a>
				</li>
				<!-- <li class="nav-item">
					<a class="nav-link" href="<?php //echo base_url();?>ClienteCtrl"><b>Recepción</b></a>
				</li> -->
				<li class="nav-item">
					<a class="nav-link white-text" href="<?php echo base_url();?>Usuario/CargarUsuario"><b>Usuarios</b></a>
				</li>
				<li class="nav-item">
					<?php if ($this->session->userdata('rol')==='Administrador'): ?>
						<a class="nav-link white-text" href="<?php echo base_url();?>Rol_Controller/Listar"><b>Roles</b></a>
					<?php endif ?>
				</li>
			</ul>
			<ul class="nav-item">
				<li>
					<a class="dropdown-trigger" data-target="dropdown1">
						<i style="margin-top: 5px;" class="large material-icons left">account_circle</i>
						<b><?php echo $this->session->userdata('nombre'); ?></b>
						<i class="material-icons right">arrow_drop_down</i>
					</a>
				</li>
				<div class="btn-grounp-sm mr-4" role="grounp">
					<ul id="dropdown1" class="dropdown-content center-text">
						<?php if ( $this->session->userdata('usuario')): ?>
							<li><a href="<?php echo base_url(); ?>Login_Controller/Logout"><i class="material-icons right">forward</i>Cerrar Sesión</a></li>
							<?php else: ?>
								<li><a href="<?php echo base_url(); ?>Login_Controller/Index">Logear</a></li>
							<?php endif ?>
						</ul>
					</div>
				</ul>
			</div>
		</nav>
	<?php endif; ?>
	<!-- Nav session Recep & Caje -->
	<?php if ($this->session->userdata('rol')=='Recepcionista' || $this->session->userdata('rol')=='Cajero'): ?>
	<!-- Dropdown Structure -->
	<ul id="dropdown1" class="dropdown-content">
		<li>
			<a class="blue-text" href="<?php echo base_url(); ?>/Login_Controller/Logout"><i class="material-icons right">forward</i>Cerrar Sesión</a>
		</li>
	</ul>
	<nav class="navbar navbar-expand-lg bg black darken-4">
		<a class="navbar-brand"><img style="height: 175px;" src="<?php echo base_url()?>assets/t2.png"></a>
		<ul>
			<div class="btn-grounp-sm mr-4" role="grounp">
				<ul>
					<li>
						<a class="dropdown-trigger" data-target="dropdown1">
							<i class="small material-icons left" style="margin-top: 5px;">account_circle</i>
							<b> <?php echo $this->session->userdata('nombre');?></b>
							<i class="material-icons right">arrow_drop_down</i></a>
						</li>
					</ul>
				</div>
			</ul>
		</nav>
	<?php endif ?>

	<!-- Diseñedares -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<!-- GoogleIcons -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!-- Framework Material desing -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>materialize/materialize/css/materialize.min.css">
	<!-- jQuery -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.4.1.min.js"></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
