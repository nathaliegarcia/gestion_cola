<!DOCTYPE html>
<html>
<head>
	<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/Captura.PNG">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!-- Framework Material desing -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>materialize/materialize/css/materialize.min.css">
	<!-- jQuery -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.4.1.min.js"></script>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<title>Iniciar Sesión</title>
	<style type="text/css">
		#lgon{
			margin-top: 100px;
		}
		body{
			background-image: linear-gradient(to right, black,gray,gray,black);
			background-size: cover;
		}

		::-webkit-input-placeholder {
			color: black;
			font: bold;
		}

		@fortawesome/free-solid-svg-icons
		@fortawesome/free-regular-svg-icons
	</style>
</head>
<body>
	<center>
		<div class="container" style="margin-top: 150px;">
			<div class="row">
				<div class="col m3"></div>
				<div class="card col m5 grey lighten-2">
					<div style="width: 300px; height: 150px;" ><img src="<?php echo base_url(); ?>assets/t.png">
					</div>
					<div class="card-content">
						<form method="post" action="<?php echo base_url(); ?>Login_Controller/Logear">
							<div class="input-field">
								<i style="color: #00695c" class="material-icons prefix">account_circle</i>
								<input type="text" style="color: black;" name="usuario" placeholder="Usuario">
							</div>

							
							<div class="input-field">
								<i style="color: #00695c;" class="material-icons prefix">vpn_key</i>
								<input id="txtPassword" type="password" style="color: black;" name="contrasena" placeholder="Contraseña">
								<button id="show_password" class="btn-small right"style="background-color: #00695c;color: white;position: relative;margin-top: -50px;" type="button" onclick="mostrarPassword()">
									<i id="icon" class="small material-icons">visibility</i></button>
								</div>
								<div><span class="bg red white-text"><?php echo $this->session->flashdata('msg'); ?></span></div>
								<div class="input-field">
									<input type="submit" class="btn btn teal darken-3" value="Iniciar Sesión">
								</div>
							</form>
						</div>
					</div>
					<div class="col m4"></div>
				</div>
			</div>
		</div>
	</center>
	<script type="text/javascript">
		if(history.forward(1)){
			location.replace(history.forward(1));
		}
	</script>
	<script type="text/javascript">
		function mostrarPassword(){
			var cambio = document.getElementById("txtPassword");
			if(cambio.type == "password"){
				cambio.type = "text";
				$('#icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
			}else{
				cambio.type = "password";
				$('#icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
			}
		}
	</script>
</body>
</html>