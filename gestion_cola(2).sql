-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-08-2019 a las 23:52:42
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestion_cola`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_generar_codigo` (OUT `p1` VARCHAR(7), IN `tr` INT)  BEGIN
    DECLARE contador INT;
    DECLARE pc VARCHAR(2) default '';
    
 IF (tr = 1) THEN
       SET pc= CONCAT(pc,'RM');
      ELSE IF(tr = 2) THEN
        SET pc= CONCAT(pc,'RT');
      ELSE IF(tr = 3) THEN
         SET pc= CONCAT(pc,'AB');
         ELSE IF(tr = 4) THEN
         SET pc= CONCAT(pc,'PR');
       END IF;
       END IF;
        END IF;
        END IF;      

    SET contador= (SELECT COUNT(*)+1 FROM cliente WHERE idtransferencia = tr); 
        IF(contador<10)THEN
           SET p1=  CONCAT(pc,'00',contador);
        ELSE IF(contador<100) THEN
           SET p1=  CONCAT(pc,'0',contador);
        ELSE IF(contador<1000)THEN
           SET p1=  CONCAT(pc,contador);
        END IF;
        END IF;
        END IF; 
   
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_insert_cliente` (IN `fecha` DATE, IN `transferencia` INT)  BEGIN
    DECLARE p1 VARCHAR(6);
  
    BEGIN
       CALL sp_generar_codigo(@p1,transferencia);
       INSERT INTO cliente(fecha,idtransferencia,codigo)VALUES(fecha,transferencia,@p1);
       select codigo from cliente where codigo = @p1;
    END;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cajero`
--

CREATE TABLE `cajero` (
  `Id_Cajero` int(11) NOT NULL,
  `N_caja` int(11) NOT NULL,
  `idestado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cajero`
--

INSERT INTO `cajero` (`Id_Cajero`, `N_caja`, `idestado`) VALUES
(1, 1, 2),
(2, 2, 2),
(3, 3, 2),
(4, 4, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `idtransferencia` int(11) NOT NULL,
  `codigo` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `idestado` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idcliente`, `fecha`, `idtransferencia`, `codigo`, `idestado`) VALUES
(1, '2019-08-15', 3, 'AB001', 1),
(2, '2019-08-15', 3, 'AB002', 1),
(3, '2019-08-15', 1, 'RM001', 1),
(4, '2019-08-15', 1, 'RM002', 1),
(5, '2019-08-15', 2, 'RT001', 1),
(6, '2019-08-15', 2, 'RT002', 1),
(7, '2019-08-15', 4, 'PR001', 1),
(8, '2019-08-15', 4, 'PR002', 1);

--
-- Disparadores `cliente`
--
DELIMITER $$
CREATE TRIGGER `eliminar` AFTER UPDATE ON `cliente` FOR EACH ROW BEGIN
INSERT INTO historial_de_clientes SET idcliente = old.idcliente, idtransferencia = old.idtransferencia, fecha = old.fecha, codigo = old.codigo, idestado =3;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE `estado` (
  `idestado` int(11) NOT NULL,
  `estado` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`idestado`, `estado`) VALUES
(1, 'Ocupado'),
(2, 'Disponible'),
(3, 'No Disponible');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_cliente`
--

CREATE TABLE `estado_cliente` (
  `idestado` int(11) NOT NULL,
  `estado` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `estado_cliente`
--

INSERT INTO `estado_cliente` (`idestado`, `estado`) VALUES
(1, 'En espera'),
(2, 'En caja'),
(3, 'Atendido'),
(4, 'No Presente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_de_clientes`
--

CREATE TABLE `historial_de_clientes` (
  `idhistorial_de_cliente` int(11) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `idtransferencia` int(11) NOT NULL,
  `codigo` varchar(11) NOT NULL,
  `fecha` date NOT NULL,
  `idestado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `historial_de_clientes`
--

INSERT INTO `historial_de_clientes` (`idhistorial_de_cliente`, `idcliente`, `idtransferencia`, `codigo`, `fecha`, `idestado`) VALUES
(1, 1, 3, 'AB001', '2019-08-15', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id_rol` int(11) NOT NULL,
  `rol` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `escribir` varchar(10) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `modificar` varchar(10) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `eliminar` varchar(10) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id_rol`, `rol`, `escribir`, `modificar`, `eliminar`) VALUES
(1, 'Administrador', 'si', 'si', 'si'),
(2, 'Recepcionista', 'si', 'no', 'no'),
(3, 'Gerente', 'si', 'no', 'no'),
(4, 'Cajero', 'si', 'si', 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transferencias`
--

CREATE TABLE `transferencias` (
  `idtransferencia` int(11) NOT NULL,
  `transferencia` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `transferencias`
--

INSERT INTO `transferencias` (`idtransferencia`, `transferencia`) VALUES
(1, 'Remesas'),
(2, 'Retiro'),
(3, 'Abono'),
(4, 'Prestamos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL,
  `usuario` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `contrasena` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuario`, `usuario`, `contrasena`, `nombre`, `id_rol`) VALUES
(1, 'Admin', 'YTEy', 'Administrador', 1),
(2, 'Grnt', 'ZzEy', 'Gte. Portillo', 3),
(3, 'Recep', 'cjEy', 'Recep. Rivas', 2),
(4, 'Recep2', 'cjEyMw==', 'Recep. García', 2),
(5, 'Gerson', 'RzEy', 'Cajero N°1', 4),
(6, 'Nicolle', 'TjEy', 'Cajero N° 2', 4),
(7, 'Kevin', 'SzEy', 'Cajero N° 3', 4),
(8, 'Daniel', 'RDEy', 'Cajero N° 4', 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cajero`
--
ALTER TABLE `cajero`
  ADD PRIMARY KEY (`Id_Cajero`),
  ADD KEY `idestado` (`idestado`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`),
  ADD KEY `idtransferencia` (`idtransferencia`),
  ADD KEY `idestado` (`idestado`);

--
-- Indices de la tabla `estado`
--
ALTER TABLE `estado`
  ADD PRIMARY KEY (`idestado`);

--
-- Indices de la tabla `estado_cliente`
--
ALTER TABLE `estado_cliente`
  ADD PRIMARY KEY (`idestado`);

--
-- Indices de la tabla `historial_de_clientes`
--
ALTER TABLE `historial_de_clientes`
  ADD PRIMARY KEY (`idhistorial_de_cliente`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `transferencias`
--
ALTER TABLE `transferencias`
  ADD PRIMARY KEY (`idtransferencia`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuario`),
  ADD KEY `id_rol` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cajero`
--
ALTER TABLE `cajero`
  MODIFY `Id_Cajero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `estado`
--
ALTER TABLE `estado`
  MODIFY `idestado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `estado_cliente`
--
ALTER TABLE `estado_cliente`
  MODIFY `idestado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `historial_de_clientes`
--
ALTER TABLE `historial_de_clientes`
  MODIFY `idhistorial_de_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `transferencias`
--
ALTER TABLE `transferencias`
  MODIFY `idtransferencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cajero`
--
ALTER TABLE `cajero`
  ADD CONSTRAINT `cajero_ibfk_1` FOREIGN KEY (`idestado`) REFERENCES `estado` (`idestado`);

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `cliente_ibfk_3` FOREIGN KEY (`idestado`) REFERENCES `estado_cliente` (`idestado`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `cliente_ibfk_4` FOREIGN KEY (`idtransferencia`) REFERENCES `transferencias` (`idtransferencia`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `roles` (`id_rol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
