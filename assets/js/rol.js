/* Insertar nuevo rol */
$('#NewRol').click(function () {
	var rol = $('#rol').val(),
	escribir = $('#escribir').val(),
	modificar = $("#modificar").val(),
	eliminar = $("#eliminar").val()

	$.ajax({
		dataType:'json',
		url: baseurl+'Rol_Controller/InsertNuevoRol',
		type: 'post',
		dataType: 'json',
		data:{
			rol, escribir, modificar, eliminar
		},
		dataType: 'json',
		beforesend:function () {
			// gif load
		},
		success: function (data) {
			if (data.success===1) {
				alert('Insert Succes!');
				document.location.href=baseurl+'Rol_Controller/Listar';
			}else{
				alert('Error al insertar');
			}
		},
		error: function (e) {
			alert('Error fatal consulte con su administrador');
			console.log(e);
		}
	});
});

/* Actualizacion de rol*/

$("#UpdateRol").click(function() {
	var id_rol = $('#id_rol').val(), 
	rol = $('#rol').val(),
	escribir = $('#escribir').val(),
	modificar = $("#modificar").val(),
	eliminar = $("#eliminar").val()

	$.ajax({
		dataType: 'json',
		url: baseurl+'Rol_Controller/ActualizarRol',
		type: 'post',
		dataType: 'json',
		data:{
			id_rol,rol, escribir, modificar, eliminar
		},
		dataType:'json',
		beforesend:function () {
			//Gif load
		},
		success:function(data) {
			if (data.success===1) {
				alert('Datos actualizados con exito!');
				document.location.href=baseurl+'Rol_Controller/Listar';
			}else{
				alert('Ocurrio un error!');
			}
		},
		error:function (e) {
			alert('Error fatal consulte con su administrador');
			console.log(e);
		}
	});
});
