$('#agregarusuario').click( function() {
var usuario = $('#usuario').val(),
	contrasena = $('#contrasena').val(),
	nombre = $('#nombre').val(),
	id_rol = $('#id_rol').val();

	$.ajax({
		dataType: 'json',
		url: baseurl+'Usuario/InsertarUsuario',
		type: 'post',
		dataType: 'json',
		data:{
			usuario,contrasena,nombre,id_rol
		},
		dataType: 'json',
		beforesend: function(){

		},
		success: function(data){
			if(data.success === 1){
				alert('Usuario insertado');
				document.location.href=baseurl+'Usuario/CargarUsuario';
			}
			else
			{
				alert('No se pudo registrar el usuario');
			}
		},
		error: function(e){
			alert('¡¡Algo fallo. Verifica el procedimiento!!');
			console.log(e);
		}
	});
});


$('#actusuario').click( function() {
var usuario = $('#usuario').val(),
	contrasena = $('#contrasena').val(),
	nombre = $('#nombre').val(),
	id_rol = $('#id_rol').val(),
	idusuario = $('#idusuario').val();

	$.ajax({
		dataType: 'json',
		url: baseurl+'Usuario/UpdateUsuario',
		type: 'post',
		dataType: 'json',
		data:{
			usuario,contrasena,nombre,id_rol,idusuario
		},
		dataType: 'json',
		beforesend: function(){

		},
		success: function(data){
			if(data.success === 1){
				alert('Datos actualizados');
				document.location.href=baseurl+'Usuario/CargarUsuario';
			}
			else
			{
				alert('No se pudieron actualizar los datos');
			}
		},
		error: function(e){
			alert('¡¡Algo fallo. Verifica el procedimiento!!');
			console.log(e);
		}
	});
});