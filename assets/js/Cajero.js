$('#Agregar').click(function(){
	var 
	N_Caja = $('#Numero').val(),
	idestado = $('#Estado').val();


	var banderaRBTN = false;

	if(N_Caja == null || N_Caja.length == 0 || /^\s+$/.test(N_Caja)){
		alert('ERROR: El campo Numero de la caja no debe ir vacío.');
		return false;
	}

	if(idestado == null || idestado.length == 0 || /^\s+$/.test(idestado)){
		alert('ERROR: El campo idestado no debe ir vacío.');
		return false;
	}


	$.ajax({
		dataType: 'json',
		url:baseurl+'Cajero/Agregar_Cajero',
		type: 'post',
		dataType: 'json',
		data:{
			N_Caja, idestado
		},

		dataType: 'json',

		beforesend: function(){

		},

		success: function(data){
			if(data.success === 1){
				alert("Registro ingresado");

				document.location.href=baseurl+'Cajero/';
			}else{
				alert("No se pudo ingresar el registro");
			}
		},

		error:function(e){
			alert("Algo Fallo consulte con el administrador");
			console.log(e);
		}
	});
});

//Actualizar cajero.
$('#Actualizar').click(function(){
	var Id_Cajero = $('#Id_Cajero').val(),
	idestado = $('#Estado').val(),
	N_Caja = $('#Numero').val();

	var banderaRBTN = false;

	if(N_Caja == null || N_Caja.length == 0 || /^\s+$/.test(N_Caja)){
		alert('ERROR: El campo N_Caja no debe ir vacío.');
		return false;
	}  
	if(idestado == null || idestado.length == 0 || /^\s+$/.test(idestado)){
		alert('ERROR: El campo idestado no debe ir vacío.');
		return false;
	}  

	$.ajax({
		dataType: 'json',
		url:baseurl+'Cajero/Actualizar_Cajero',
		type: 'post',
		dataType: 'json',
		data:{
			Id_Cajero,  N_Caja, idestado
		},

		dataType: 'json',

		beforesend: function(){

		},

		success: function(data){
			if(data.success === 1){
				alert("Atualizado Correctamente");

				document.location.href=baseurl+'Cajero/';
			}else{
				alert("No se pudo Actualizar");
			}
		},

		error:function(e){
			alert("Algo Fallo consulte con el administrador");
			console.log(e);
		}
	});
});
///////////////////////
$('#libre').click(function(){
	var 
	idcliente = $('#idcliente').val(),
	fecha =$('#fecha').val(),
	idtransferencia =$('#Transferecia').val(),
	codigo = $('#Codigo').val(),
	idestado =$('#Estado').val();

	$.ajax({
		dataType: 'json',
		url:baseurl+'Cajero/transferecia_cliente',
		type: 'post',
		dataType: 'json',
		data:{
			idcliente, fecha, idtransferencia, codigo, idestado
		},

		dataType: 'json',

		beforesend: function(){

		},

		success: function(data){
			if(data.success === 1){
				//alert("Cliente en Caja");

				document.location.href=baseurl+'Cajero/cliente';
			}else{
				alert("No se pudo Actualizar");
			}
		},

		error:function(e){
			alert("Algo Fallo consulte con el administrador");
			console.log(e);
		}
	});
});
///////
/*$('#boton').click(function(){
	alert('codigo');
});*/